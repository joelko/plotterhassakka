// PlotterCommandParserVersionAlpha.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include <string> //needed for FULL APP certainly(?)
#include <sstream>//maybe needed for FULL APP TODO:: confirm it later ?!?!
#include <vector>//needed for FULL APP certainly(?)
#include <fstream>//needed for debugging version only
#include<iostream>//needed for debugging version only
using namespace std;

/*BASIC IDEA OF PARSER ALGORITHM

this is for debug mode
1) open file for reading
2) getline from the file to rawCommandString until end
3) for each rawCommandString- we will getline into that string with delimiter spacebar
4) the gottenLines will be transferred into vector<string> vec 
5) we can process easily the individual cpp strings from the vector
5) only the G1 move command will be more difficult to parse, because must be able to find positive or negative coord and the tensplace and hundredthsplace
6) when results are gotten from parser, then vec must be flushed and variables reset
*/

/*Custom string_splitter function,

maybe works, maybe it doesn't work, purpose is to also split string, 
but also take into account that if the string ends in single delimit character, 
-> then you have to push empty cpp string into the vector at that point... because otherwise
illegal commands with single ending spacebar as the main cause, those would be categorized as legal command
legal commands cannot end in the word-by-word delimiting character, because the getline gets each line
without expecting the word-by-word delimiter at the end... */

bool assertNotLeadingZero(const char &letter) {
	if (isdigit(letter) && letter != '0') {
		return true;
	}
	else { return false; }
}


std::vector<std::string> split_string(const std::string &strRef, const char delimit) {
	std::vector<std::string> vec;
	auto res = strRef.find(delimit);
	int searchind = 0;
	int pos = 0;
	int size = strRef.length();

	if (res == std::string::npos) {  //didn't find delimiter ->put string to vec and return it early.
		vec.push_back(strRef);
		return vec;
	}
	else {
		while ( strRef.find(delimit, searchind)  != std::string::npos) { //maybe code can be made to look nicer for the condition but dont refactor yet
			pos = strRef.find(delimit, searchind);
			if (searchind == pos) {
				vec.push_back(std::string(""));
				++searchind;
			}
		    else if (pos != std::string::npos) {
				vec.push_back(strRef.substr(searchind, pos - searchind));
				searchind = pos + 1;
			}
			/*if ( pos == std::string::npos ){ maybe this branch was in the wrong place, who knows only God knows if this is needed!
				pos = size;
				vec.push_back(strRef.substr(searchind, pos - searchind));
				return vec;
			}*/
		}
		if (searchind < size) {
		//get last valid element, when the rawstr doesnt end in the delimitin char
			vec.push_back(strRef.substr(searchind, size - searchind));
		}
		else if (strRef[size-1] == delimit)//check if last index char was == delimit -> if so ->push empty string and return
			vec.push_back(std::string(""));
		return vec;		
	}
}

vector<string> splitStringStream( const string &strInput, const char delimit) {
	vector<string> vec;
	istringstream iss(strInput);
	string temp;
	while (getline(iss, temp, delimit ) ) {
		vec.push_back(temp);
	}
	if( strInput[strInput.length()-1] == delimit ) { //check out if this code works or not!?!? debugging
		vec.push_back("");
	}

	return vec;
	//note this fucntion doesnt work completely because when inputstr last char == delimit
	//-> it is disregarded,
	//iNSTEAD what should happen is that when last char is delimit -> push empty string into vec
}

struct CommandStruct {
	enum {
		M1,
		M4,
		M10,
		G1,
		G28
	} commandWord;
	int commandNumber;
	bool isLegal;
	int xCoord;
	int yCoord;

	
};

bool parseM10(const vector<string>  &vecRef, CommandStruct &cmdRef) {
	auto len = vecRef.size();
	
	if (len == 1 && vecRef[0]=="M10") {
		cmdRef.commandWord = CommandStruct::M10;
		cmdRef.commandNumber = 0;
		cmdRef.isLegal = true;
		cmdRef.xCoord = 0;
		cmdRef.yCoord = 0;
		return true;
	}
	else 
		return false;
}

bool parseG28(const vector<string>  &vecRef, CommandStruct &cmdRef) {
	auto size = vecRef.size();
	if (size==1 && vecRef[0]=="G28") {
		cmdRef.commandWord = CommandStruct::G28;
		cmdRef.commandNumber = 0;
		cmdRef.isLegal = true;
		cmdRef.xCoord = 0;
		cmdRef.yCoord = 0;
		return true;
	}
	else {
		return false;
	}

}

bool parseM4(const vector<string>  &vecRef, CommandStruct &cmdRef) {
	auto size = vecRef.size();
	uint8_t comNum;
	
	if(size==2 && vecRef[0]=="M4"   ){
		std::string temp(vecRef[1]);
		if ( temp.size() <= 3 && temp.size() >= 1) {
			//check if the numberParameter is legal value
			auto digitcount = temp.size();
			bool legalNumber = false;
			if (digitcount==1) {
				legalNumber=isdigit(temp[0]);
			}
			else if (digitcount == 2) {
				legalNumber = (assertNotLeadingZero(temp[0]) && isdigit(temp[1]) );
			}
			else if (digitcount==3) {
				legalNumber = (assertNotLeadingZero(temp[0])  && isdigit(temp[1]) && isdigit(temp[2]) );
			}
			if (legalNumber) {
				istringstream(temp) >> comNum;
				cmdRef.commandWord = CommandStruct::M4;
				cmdRef.commandNumber = comNum;
				cmdRef.isLegal = true;
				cmdRef.xCoord = 0;
				cmdRef.yCoord = 0;
				return true;
			}
		}
	}

	return false;
}

bool parseM1(const vector<string>  &vecRef, CommandStruct &cmdRef) {
	auto size = vecRef.size();
	uint8_t comNum;
	if (size==2 && vecRef[0]=="M1") {
		std::string candidatestr(vecRef[1]);
		auto digitcount = vecRef[1].size();
		bool legalNumber = false;
		if (digitcount==1) {
			legalNumber = (  isdigit(candidatestr[0])  );
		}
		else if (digitcount==2) {
			legalNumber = ( assertNotLeadingZero(candidatestr[0]) && isdigit(candidatestr[1]) );
		}
		else if (digitcount==3) {
			legalNumber = ( assertNotLeadingZero(candidatestr[0]) && isdigit(candidatestr[1]) && isdigit(candidatestr[2]) );
		}

		if (legalNumber) {
			istringstream(candidatestr) >> comNum;
			cmdRef.commandWord = CommandStruct::M1;
			cmdRef.commandNumber = comNum;
			cmdRef.isLegal = true;
			cmdRef.xCoord = 0;
			cmdRef.yCoord = 0;
			return true;
		}
	}

	return false;

}


//THIS HELPER FuNCTION IS BUGGED SORRY!!!
bool parseG1Parameter(const std::string &str, int &parameter, const char &axisChar) {
	
	//theAxis either Y or X
	auto size = str.length();
	bool isLegalParameter = false;

	if (size <= 8 && size >= 5) {//is of legal size
		if (str[0] == axisChar) {//has correct axis Letter X or Y
			if (str[1] == '-') {//negative numbers
				if (size == 6 &&
					isdigit(str[2]) &&
					str[3] == '.' &&
					isdigit(str[4]) &&
					isdigit(str[5])
					) {
					isLegalParameter = true;
				}
				else if (size==7 &&
					assertNotLeadingZero(str[2]) &&
					isdigit(str[3]) &&
					str[4]=='.' &&
					isdigit(str[5]) &&
					isdigit(str[6])
					) {
					isLegalParameter = true;
				}
				else if (size==8 && 
					assertNotLeadingZero(str[2]) && 
					isdigit(str[3]) && 
					isdigit(str[4]) && 
					str[5]=='.' && 
					isdigit(str[6]) && 
					isdigit(str[7])    
					) {
					isLegalParameter = true;
				}
			}
			else if (isdigit(str[1])) {//positive numbers
				if (size == 5 &&
					isdigit(str[1]) &&
					str[2] == '.' &&
					isdigit(str[3]) &&
					isdigit(str[4])
					) {
					isLegalParameter = true;
				}
				else if (size == 6 &&
					assertNotLeadingZero(str[1]) &&
					isdigit(str[2]) &&
					str[3] == '.' &&
					isdigit(str[4]) &&
					isdigit(str[5])
					) {
					isLegalParameter = true;
				}
				else if (size == 7 &&
					assertNotLeadingZero(str[1]) &&
					isdigit(str[2]) &&
					isdigit(str[3]) &&
					str[4] == '.' &&
					isdigit(str[5]) &&
					isdigit(str[6])
					) {
					isLegalParameter = true;
				}
			}
		}
		
	}
	
	return isLegalParameter;
	
}

bool parseG1(const vector<string> &vecRef, CommandStruct &cmdRef) {
	auto size = vecRef.size();
	int possibleXCoord;
	int possibleYCoord;
	bool isLegal = false;

	if (size == 4) {
		if (vecRef[0] == "G1") {
			if (  parseG1Parameter( vecRef[1], possibleXCoord, 'X' ) && parseG1Parameter( vecRef[2], possibleYCoord, 'Y' )  ) {//parse xcoord and ycoord
				if (vecRef[3]=="A0") {
					isLegal = true;
					cmdRef.commandWord = CommandStruct::G1;
					cmdRef.xCoord = possibleXCoord;
					cmdRef.yCoord = possibleYCoord;
					cmdRef.isLegal = true;
					cmdRef.commandNumber = 0;
				}
			}
		}
	}

	return isLegal;
}


int main()
{
	int linecount = 0;
	ifstream gcodefile1("unit test inputs ASSERT==TRUE.txt");
	ofstream resultsfile1("results==TRUE.txt");
	string rawCommandLine;
	while (getline(gcodefile1, rawCommandLine)) {
		++linecount;
		/*variables should be reseted with each iteration if they are declared in the loop like this (?)  */
	/*	string rawCommandLine="";*/
		vector<string> vec;
		vector<string> vec2;
		string temp;
		string temp2;
		//cout << "enter command: "<<endl;
		;//get userinputs line-by-line
		//cout << "rawCommand was== " << rawCommandLine << endl; //debug printing!
		temp2=rawCommandLine;
		//vec2 = split_string(temp2, ' ');
		vec = splitStringStream( rawCommandLine, ' ' );
		//cout << "iss values were:" << endl;
		//for (auto i = vec.begin(); i != vec.end(); i++) {
		//	cout << *i;
		//	cout << "::";// doublecolon limiter is easier to see between the elements of the vector 	
		//}
		//cout<< endl;
		//cout << "stringSplitAlgo values were:" << endl;
		//for (auto n = vec2.begin(); n != vec2.end(); n++) {
		//	cout << *n;
		//	cout << "::";
		//}
		//cout << endl;
		//cout << " ;; ROUND FINISHED ;; " << endl;
		CommandStruct cmd;
		bool isLegal = parseG28(vec, cmd);
		if (!isLegal) {
			isLegal = parseM10(vec,cmd);
		}
		if (!isLegal) {
			isLegal = parseM1(vec,cmd);
		}
		if (!isLegal) {
			isLegal = parseM4(vec,cmd);
		}
		if (!isLegal) {
			isLegal = parseG1(vec, cmd);

		}

		if (cmd.isLegal && isLegal) {//write results into a results file and newline
			resultsfile1 << "TRUE, line  ";
			resultsfile1 << linecount;
			resultsfile1 << "\n";
		}
		else {
			resultsfile1 << "FALSE, line  ";
			resultsfile1 << linecount;
			resultsfile1 << "\n";
		}


		/*TODO::
		 each loop tests one command if it's legal or not -> set variable true when legality is verified, otherwise dont touch boolean yet
		 Ok, so if the first loop is not match -> then you check the second loop and it is matched -> switch boolean to true ->
		 -> then you know already that the later loops will not be matches anymore ->they dont nneed to be checked anymore
		 while(!legal){
		  check entire vector and all its strings for M1 command
		 }
		 while (!legal){
		 check entire vector and all its strings for M4 command
		 }
		 etc...
		 check G1 command at the last stage for clarity maybe...
		 checking G1 command is trickiest...*/ 
		vec.clear();//flush vector contents
		vec2.clear();
		temp2 = "";
		rawCommandLine = "";
		/*cout << endl;*/
	}
    return 0;
}

